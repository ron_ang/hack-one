# README #

Learn basic Android reverse engineering https://ronillo-ang.blogspot.com/2020/10/android-reverse-engineering-beginners.html

Think like as an adversary https://ronillo-ang.blogspot.com/2020/10/beginners-guide-think-like-and-act-like.html

### What is this repository for? ###

* This repository constains android applications for you to reverse engineer. Break and alter the code without the actual project source code.
* Version 1.8

### Objective ###

#### Tasks ####
* Modify the [unlockme-debug.apk](https://bitbucket.org/ron_ang/hack-one/downloads/unlockme-debug.apk) to bypass the verification code or accept any input.

#### Advance Tasks ####
* Modify the [Login-CTF.apk](https://bitbucket.org/ron_ang/hack-one/downloads/Login-CTF.apk) to bypass the login.
* Modify the [Login-CTF.apk](https://bitbucket.org/ron_ang/hack-one/downloads/Login-CTF.apk) to steal the correct username and password, then forward it to your own server.

You can wrote a blog about this basic CTF hacking challenge.